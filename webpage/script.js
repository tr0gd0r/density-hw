(function () {
  const table = document.getElementById("table-body")

  const clearTable = function() {
    Array.from(table.children).forEach((el, index) => {
      el.remove()
    })
  }

  const buildTable = function(sensorCheckIns) {
    var fragment = new DocumentFragment()
    sensorCheckIns.forEach(function(sensorCheckIn) {
        const tr = document.createElement("tr")

        const sensor = document.createElement("td")
        sensor.innerText = sensorCheckIn["sensor"]
        tr.appendChild(sensor)

        const time = document.createElement("td")
        time.innerText = sensorCheckIn["check_in"]
        tr.appendChild(time)
        
        fragment.appendChild(tr)
    })
    table.appendChild(fragment)
  }

  const getSensorData = function() {
    const endpoint = "http://localhost:5000/heartbeats"
    fetch(endpoint).then(function(res) {
      res.json().then(function(data) {
        clearTable()
        buildTable(data)
      })
    })
  }

  setInterval(getSensorData, 1000)
})();
