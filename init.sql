create table heartbeat (
  sensor_id uuid primary key,
  check_in timestamp not null default now()
);
