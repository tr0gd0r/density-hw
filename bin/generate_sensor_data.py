import argparse
import random
from time import sleep
import uuid
import urllib.request


def heartbeat(sensor_id):
    res = urllib.request.urlopen(f'http://localhost:5000/heartbeat/{sensor_id}')
    if res.status != 200:
        raise Exception('heartbeat returned non 200 status code')


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--sensors", type=int, default=10, help="number of sensors to make heartbeat data for")
    parser.add_argument("--withhold", type=int, default=2, help="number of sensors to stop sending data for. useful for testing alarm functionality")
    args = parser.parse_args()

    sensors = []
    for _ in range(args.sensors):
        sensor_id = str(uuid.uuid4())
        sensors.append(sensor_id)

    # send inital data for each sensor
    for s in sensors:
        heartbeat(s)

    # drop however many sensors we're witholding
    for _ in range(args.withhold):
        sensors.pop()

    # randomly just send heartbeats
    try:
        while True:
            s = random.choice(sensors)
            heartbeat(s)
            sleep(0.5)
    except KeyboardInterrupt:
        return


if __name__ == '__main__':
    main()
