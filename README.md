## Dependencies
* python3
* docker
* docker-compose

## Installation
```bash
docker-compose up
```
This will launch postgres, flask api and nginx.

## Running it

The webpage is available at `http://localhost:9999/`. The table will be empty until you start sending heartbeats.

To populate with sensor data, run `bin/generate_sensor_data.py`. There are some args to toggle how many sensors to send (and not send) heartbeats for. This will continue to send heartbeats until you kill it.

## Alarm

There is an endpoint to return all sensors that have not checked in since some timestamp:
```bash
$ curl --silent http://localhost:5000/alarm?since=1607920365 | jq .
[
  {
    "check_in": "2020-12-14 04:32:12",
    "sensor": "1f87e33c-239b-4794-9c2d-7918629ccef1"
  },
  {
    "check_in": "2020-12-14 04:32:12",
    "sensor": "8b665747-60fc-4cb3-98e8-d2ffdbc6708e"
  },
...
```

## Endpoints

* you can empty all heartbeat data by hitting `http://localhost:5000/empty`
* send a heartbeat by hitting `http://localhost:5000/heartbeat/<sensor_id>` where `sensor_id` is a UUID
* see all heartbeats by hitting `http://localhost:5000/heartbeats`

