from datetime import datetime
import logging
import sys

from flask import Flask, jsonify, request
import psycopg2

app = Flask(__name__)
conn = psycopg2.connect("dbname='docker' user='docker' host='db' password='docker'")
logger = logging.getLogger(__name__)
stdout_handler = logging.StreamHandler(sys.stdout)
logger.addHandler(stdout_handler)


@app.route('/heartbeats')
def get_heartbeats():
    q = 'SELECT sensor_id, check_in FROM heartbeat ORDER BY sensor_id asc' 
    cur = conn.cursor()
    cur.execute(q)
    heartbeats = []
    rows = cur.fetchall()
    for row in rows:
        t = row[1].strftime("%Y-%m-%d %H:%M:%S")
        heartbeat = {'sensor': row[0], 'check_in': t}
        heartbeats.append(heartbeat)
    return jsonify(heartbeats)


@app.route('/heartbeat/<sensor>')
def add_heartbeat(sensor):
    if len(sensor) == 0:
        return '', 400
    q = '''
    INSERT INTO heartbeat (sensor_id)
    VALUES (%s)
    ON CONFLICT (sensor_id) DO UPDATE SET check_in = now()
    '''
    cur = conn.cursor()
    try:
        with conn:
             with conn.cursor() as cur:
                cur.execute(q, (sensor,))
                return '', 200
    except:
        logging.exception('error inserting heartbeat')
        return '', 400


# list sensors that haven't checked in since some time
# /alarm?since=<unix timestamp>
@app.route('/alarm')
def check_missing_sensors():
    since = request.args.get('since')
    if len(since) == 0:
        return '', 400
    try:
        ts = datetime.fromtimestamp(int(since)) 
    except:
        logging.exception('cannot parse timestamp from since param')
        return '', 400
    q = 'SELECT sensor_id, check_in FROM heartbeat where check_in <= %s' 
    cur = conn.cursor()
    cur.execute(q, (ts,))
    missing_sensors = []
    rows = cur.fetchall()
    for row in rows:
        t = row[1].strftime("%Y-%m-%d %H:%M:%S")
        sensor = {'sensor': row[0], 'check_in': t}
        missing_sensors.append(sensor)
    return jsonify(missing_sensors)


@app.route('/empty')
def truncate_heartbeats():
    q = 'TRUNCATE TABLE heartbeat' 
    try:
        with conn:
             with conn.cursor() as cur:
                cur.execute(q)
                return '', 200
    except:
        logging.exception('error inserting heartbeat')
        return '', 400


@app.after_request
def after_request(response):
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
